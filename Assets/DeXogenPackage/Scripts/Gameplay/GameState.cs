using System;
using UnityEngine;

namespace TestProjectTargem.Gameplay
{
    public class GameState
    {
        private long score;
        private float timer;

        public Action<long> ScoreChanged;
        public Action<float> TimerChanged;

        public long Score
        {
            get => score;
        }

        public void UpdateTimer()
        {
            timer += Time.deltaTime;
            TimerChanged?.Invoke(timer);
        }

        public void AddScore(int addScore = 1)
        {
            score += addScore;
            ScoreChanged.Invoke(score);
        }

        public void ResetGameState()
        {
            score = 0;
            ScoreChanged?.Invoke(score);
            timer = 0;
            TimerChanged?.Invoke(timer);
        }
    }
}
