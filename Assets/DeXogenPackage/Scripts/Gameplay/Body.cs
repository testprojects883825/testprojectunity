using UnityEngine;

namespace TestProjectTargem.Gameplay
{
    public class Body : MonoBehaviour
    {
        private float brakingDistanceSq = 10f;
        private bool isBraking = false;
        private bool isFliesOff = false;
        private Vector3 brakingStartVelocity = Vector3.zero;
        private Vector3 velocity = Vector3.zero;
        private Vector3 angularVelocity = Vector3.zero;

        [HideInInspector] public bool IsTriggered = false;

        private void FixedUpdate()
        {
            UpdateVelocity();
            UpdatePosition();
            UpdateRotation();
        }

        public void ResetBraking()
        {
            isBraking = false;
            brakingStartVelocity = Vector3.zero;
        }

        private void UpdateVelocity()
        {
            var gravity = App.Instance.Gravity;
            var gravityDirection = gravity.Point - transform.position;
            var dot = Vector3.Dot(gravityDirection, velocity);

            // if body in braking area and velocity direction looks to gravityDirection
            if (gravityDirection.sqrMagnitude < brakingDistanceSq && dot > 0 && !isFliesOff)
            {
                if (!isBraking)
                {
                    isBraking = true;
                    brakingStartVelocity = velocity;
                }

                // update velocity considering braking
                var brakingFactor = 1 - gravityDirection.sqrMagnitude / brakingDistanceSq;
                velocity = Vector3.Lerp(brakingStartVelocity, gravity.Point, brakingFactor);
            }
            else
            {
                if (isBraking)
                {
                    ResetBraking();
                }

                // update velocity considering gravity
                velocity += gravityDirection.normalized * gravity.Value * Time.fixedDeltaTime;
            }

            if (gravityDirection.sqrMagnitude > brakingDistanceSq)
            {
                isFliesOff = false;
                IsTriggered = false;
            }

            // adjust the move direction to gravityDirection
            if (dot > 0)
            {
                var velocityProject = Vector3.Project(velocity, gravityDirection.normalized);
                velocity = velocityProject.normalized * velocity.magnitude;
            }

            // clamp velocity for more predictable behavior
            velocity = Vector3.ClampMagnitude(velocity, 100f);
        }

        private void UpdatePosition()
        {
            transform.position += velocity * Time.fixedDeltaTime;
        }

        private void UpdateRotation()
        {
            transform.rotation *= Quaternion.Euler(angularVelocity);
        }

        private void OnTriggerEnter(Collider other)
        {
            other.gameObject.GetComponent<Renderer>().material.color = Color.red;
            ResetBraking();
            var impactDirection = (transform.position - other.transform.position).normalized;
            velocity = impactDirection * UnityEngine.Random.Range(10f, 30f);
            angularVelocity = (other.attachedRigidbody.transform.position - other.transform.position) * UnityEngine.Random.Range(0.1f, 1f);
            isFliesOff = true;
            if (!IsTriggered)
            {
                App.Instance.GameState.AddScore(1);
                IsTriggered = true;
                other.GetComponentInParent<Body>().IsTriggered = true;
            }
        }
    }
}
