using UnityEngine;

namespace TestProjectTargem.Gameplay
{
    public class Gravity
    {
        public Gravity(GameObject gravityCenter, float force = 9.81f)
        {
            Point = gravityCenter.transform.position;
            Value = force;
        }

        public Vector3 Point { get; private set; }
        public float Value { get; private set; }
    }
}
