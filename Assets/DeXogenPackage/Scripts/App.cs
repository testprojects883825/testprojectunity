using TestProjectTargem.Gameplay;
using UnityEngine;

namespace TestProjectTargem
{
    public class App : MonoBehaviour
    {
        [SerializeField] private GameObject gravityCenter;
        
        private void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(this);
            }
            else
            {
                Instance = this;
            }

            Gravity = new Gravity(gravityCenter);
            GameState = new GameState();
        }

        private void Update()
        {
            GameState.UpdateTimer();
        }

        public static App Instance { get; private set; }

        public Gravity Gravity { get; private set; }

        public GameState GameState { get; private set; }
    }
}
