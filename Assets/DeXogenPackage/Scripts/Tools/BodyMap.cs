﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TestProjectTargem.Tools
{
    public class BodyMap : IEnumerable<bool>
    {
        private bool[,,] bodyMap;
        private Vector3Int center;

        public BodyMap(int Radius)
        {
            var arraySize = (Radius - 1) * 2 + 1;
            bodyMap = new bool[arraySize, arraySize, arraySize];
            center = new Vector3Int(Radius - 1, Radius - 1, Radius - 1);
        }

        public Vector3Int Center => center;

        public int Length => bodyMap.Length;

        public bool this[Vector3Int coords]
        {
            get => bodyMap[coords.x, coords.y, coords.z];
            set => bodyMap[coords.x, coords.y, coords.z] = value;
        }

        public bool this[int x, int y, int z]
        {
            get => bodyMap[x, y, z];
            set => bodyMap[x, y, z] = value;
        }

        public bool this[int index]
        {
            get => this[GetCoords(index)];
            set => this[GetCoords(index)] = value;
        }

        public Vector3Int GetCoords(int index)
        {
            int depth = bodyMap.GetLength(0);
            int rows = bodyMap.GetLength(1);
            int cols = bodyMap.GetLength(2);

            if (index < 0 || index >= depth * rows * cols)
            {
                throw new ArgumentOutOfRangeException(nameof(index), "Index is out of the bounds of the array.");
            }

            int x = index / (rows * cols);
            int y = (index % (rows * cols)) / cols;
            int z = index % cols;

            return new Vector3Int(x, y, z);
        }

        public bool IsWithinBoundingBox(Vector3Int target)
        {
            var xBound = 0 <= target.x && target.x < bodyMap.GetLength(0);
            var yBound = 0 <= target.y && target.y < bodyMap.GetLength(1);
            var zBound = 0 <= target.z && target.z < bodyMap.GetLength(2);
            return xBound && yBound && zBound;
        }

        public IEnumerator<bool> GetEnumerator()
        {
            foreach (var part in bodyMap)
            {
                yield return part;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
