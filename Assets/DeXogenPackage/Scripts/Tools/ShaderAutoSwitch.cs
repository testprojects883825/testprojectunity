﻿using UnityEngine;
using UnityEngine.Rendering;

namespace TestProjectTargem.Tools
{
    [ExecuteInEditMode]
    public class ShaderAutoSwitch : MonoBehaviour
    {
        public Material MainMaterial;

        private void Awake()
        {
            if (GraphicsSettings.defaultRenderPipeline != null)
            {
                return;
            }

            if (MainMaterial.shader == Shader.Find("Standard"))
            {
                return;
            }

            Debug.LogWarning("The default render pipeline is the Built-in Render Pipeline!");

            MainMaterial.shader = Shader.Find("Standard");

            Debug.LogWarning("Materails shaders was switched!");
        }
    }
}
