﻿using System;
using System.Linq;
using TestProjectTargem.Gameplay;
using UnityEngine;

namespace TestProjectTargem.Tools
{
    public class BodyGenerator : MonoBehaviour
    {
        [Range(2,10)]
        public int Radius = 3;
        [Range(0.01f, 1f)]
        public float Density = 0.5f;
        public bool WithSphereSlicing = true;
        public GameObject PartPrefab;

        private BodyMap bodyMap;

        public void RegenerateBody()
        {
            if (!TryGetComponent<Body>(out _))
            {
                throw new Exception($"Not found {typeof(Body)} component at object: {name}!");
            }

            bodyMap = new BodyMap(Radius);

            bodyMap[bodyMap.Center] = true;
            var currentDensity = GetDensity();
            Vector3Int currentPart = bodyMap.Center;
            bool needNewPart = false;

            var counter = 0;
            while (currentDensity < Density)
            {
                // iterations constrain
                counter++;
                if (counter > 10000)
                {
                    Debug.LogWarning($"Too match iterations!");
                    break;
                }

                // if last part has no other ways
                if (needNewPart)
                {
                    if (!TryGetRandomExistsPart(out var existsPart))
                    {
                        throw new Exception("Available parts not found!");
                    }

                    currentPart = existsPart;
                    needNewPart = false;
                }

                // find empty neighbor
                if (!TryGetRandomEmptyNeighbor(currentPart, out var empty))
                {
                    Debug.Log("Empty cell near exist part not found! Reset target part.");
                    needNewPart = true;
                    continue;
                }

                bodyMap[empty] = true;
                currentPart = empty;

                currentDensity = GetDensity();
            }

            if (WithSphereSlicing)
            {
                SphereSliceBody();
            }

            CreateBody(bodyMap);
        }

        private void SphereSliceBody()
        {
            // slice body angles to be like sphere
            for (int k = 0; k < bodyMap.Length; k++)
            {
                if (!bodyMap[k])
                {
                    continue;
                }

                var partCoords = bodyMap.GetCoords(k);
                var magnitude = (bodyMap.Center - partCoords).magnitude;
                if (magnitude > Radius)
                {
                    bodyMap[partCoords] = false;
                }
            }
        }

        private void CreateBody(BodyMap bodyMap)
        {
            ClearBody();

            for (int k = 0; k < bodyMap.Length; k++)
            {
                if (bodyMap[k])
                {
                    var part = Instantiate(PartPrefab, transform);
                    var bodyOffset = new Vector3Int(Radius - 1, Radius - 1, Radius - 1);
                    part.transform.localPosition = bodyMap.GetCoords(k) - bodyOffset;
                }
            }
        }

        private void ClearBody()
        {
            // clear parts of old body
            for (int k = transform.childCount - 1; k >= 0; k--)
            {
                DestroyImmediate(transform.GetChild(k).gameObject);
            }
        }

        private float GetDensity()
        {
            // return density - relation of current count part to length of bodyMap array;
            return (float)bodyMap.Where(item => item).Count() / (float)bodyMap.Length;
        }

        private bool TryGetRandomExistsPart(out Vector3Int coords)
        {
            var randomIndex = UnityEngine.Random.Range(0, bodyMap.Length);

            for (int k = 0; k < bodyMap.Length; k++)
            {
                // n - cyclic index
                var n = (randomIndex + k) % bodyMap.Length;

                if (bodyMap[n] && TryGetRandomEmptyNeighbor(bodyMap.GetCoords(n), out _))
                {
                    coords = bodyMap.GetCoords(n);
                    return true;
                }
            }

            coords = new Vector3Int(-1, -1, -1);
            return false;
        }

        private bool TryGetRandomEmptyNeighbor(Vector3Int target, out Vector3Int coords)
        {
            var randomIndex = UnityEngine.Random.Range(0, 27);

            for (int k = 0; k < 27; k++)
            {
                // check k - is index of central cell of 3x3 cube
                if (k == 13)
                {
                    continue;
                }

                // n - cyclic index
                var n = (randomIndex + k) % 27;

                var coordsOffset = GetCoordsOffset(n);

                // check current cell - is in body array bounds
                if (!bodyMap.IsWithinBoundingBox(target + coordsOffset))
                {
                    continue;
                }

                if (!bodyMap[target + coordsOffset])
                {
                    coords = target + coordsOffset;
                    return true;
                }
            }

            coords = new Vector3Int(-1, -1, -1);
            return false;

            Vector3Int GetCoordsOffset(int index)
            {
                int x = index / (3 * 3) - 1;
                int y = (index % (3 * 3)) / 3 - 1;
                int z = index % 3 - 1;

                return new Vector3Int(x, y, z);
            }
        }
    }
}
