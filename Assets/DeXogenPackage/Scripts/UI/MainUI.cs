using UnityEngine;
using UnityEngine.UI;

namespace TestProjectTargem.UI
{
    public class MainUI : MonoBehaviour
    {
        [SerializeField] private Text timerText;
        [SerializeField] private Text scoreText;
        [SerializeField] private Button resetButton;

        private void Start()
        {
            App.Instance.GameState.TimerChanged += (timer) => timerText.text = timer.ToString("F0");
            App.Instance.GameState.ScoreChanged += (score) => scoreText.text = score.ToString("F0");
            resetButton.onClick.AddListener(App.Instance.GameState.ResetGameState);
        }
    }
}
