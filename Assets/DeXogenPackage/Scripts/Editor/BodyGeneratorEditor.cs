﻿using TestProjectTargem.Tools;
using UnityEditor;
using UnityEngine;

namespace TestProjectTargem
{
    [CustomEditor(typeof(BodyGenerator))]
    public class BodyGeneratorEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            var bodyGenerator = (BodyGenerator)target;

            DrawDefaultInspector();

            if (GUILayout.Button("RegenerateBody"))
            {
                bodyGenerator.RegenerateBody();
            }
        }
    }
}
